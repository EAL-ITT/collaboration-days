# Collaboration Days evaluering

Udfyld venligst evalueringsundersøgelsen af Collaboration Days, som findes på:  
[https://forms.gle/otyMmJEGM27LmzLi7](https://forms.gle/otyMmJEGM27LmzLi7)

*Linket er ikke aktivt før afslutningen af Collaboration Days*

<!-- # Collaboration days evaluation

Please complete the Collaboration days evaluation survey located at:  
[https://forms.gle/otyMmJEGM27LmzLi7](https://forms.gle/otyMmJEGM27LmzLi7)

*The link is not active before the end of Collaboration Days*
-->