# Om

Dette websted indeholder oplysninger og øvelsesvejledninger til samarbejdsdagene på UCL - IT-teknologuddannelsen

Collaboration Days er en del af introduktionsprogrammet til IT-teknologuddannelsen på UCL. 

Collaboration Days er en blanding af holddannelse, præsentation af IT-teknologprofessionen, indhold af IT-teknolog uddannelsen samt oplæg fra ældre IT-teknolog studerende.

Formålet med Collaboration Days er at skabe relationer, fra studerende til studerende, fra studerende til undervisere og fra studerende til praksis.
Deltagelse i Collaboration Days bidrager til at give dig en god start på din uddannelse og et sted at høre til - dit team.  
Du får mere i løbet af dagene detaljeret viden om IT-teknologuddannelsens opbygning, og du får mulighed for at lære dine medstuderende at kende.  

På Collaboration Days vil der være præsentationer fra erhvervet i form af både virksomheder og færdiguddannede studerende.
Forhåbentlig vil dette give dig et mere klart billede af IT-teknologfaget, et erhverv du er på vej ind i og begynder at blive en del af.

Velkommen til Collaboration Days og velkommen til IT-teknologuddannelsen!

<!-- (This website contains information and exercise instructions for the collaboration days at UCL - IT technology programme

Collaboration Days are part of the introduction programme in IT technology at UCL. 

Collaboration Days is a mixture of team forming, presentation of the IT technology profession, contents of the IT technology education as well as presentations from older IT technology students.

The purpose of collaboration days are to establish relations, student to students, student to lecturers and student to profession.
Participation in collaboration days will give you as a student a good start at your studies and a place to belong - your team.  
You will gain more detailed knowledge about the IT Technology education structure and you will get to learn your fellow students for the next two years.  

During collaboration days there will be presentations from the profession in terms of both collaborating companies and graduated students.
Hopefully this will give you a more clear picture of the IT Technology profession, a profession you are entering and starting to become a part of.

Welcome to collaboration days and welcome to IT Technology !)

-->

