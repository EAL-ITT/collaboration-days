# Links

* Gitlab-sider - kurser i IT-teknologi [https://eal-itt.gitlab.io/](https://eal-itt.gitlab.io/)  
* Timeedit - Tidsplan [https://timeedit.net/](https://timeedit.net/)  
* Itslearning - forelæsningsplaner osv. [https://ucl.itslearning.com/](https://ucl.itslearning.com/)  
* Wiseflow - eksamener og OLA'er [https://ucl.wiseflow.dk/](https://ucl.wiseflow.dk/)   
* Google suite - docs osv. - log på med din UCL edu-mail [https://gsuite.google.com/dashboard](https://gsuite.google.com/dashboard)  
* UCL it-vejledninger [https://it.ucl.dk/](https://it.ucl.dk/)  
* Introduktion til læringsteknologi og IT på UCL [https://rise.articulate.com/share/bEm4fP8ZostGGxHfBfQcTErVhJWLOZLx#/](https://rise.articulate.com/share/bEm4fP8ZostGGxHfBfQcTErVhJWLOZLx#/)
* Introduktionsslides til samarbejdsdage [https://docs.google.com/presentation/d/1hPEU4v9iFjGo6f7lVpU5kVXb9RiOyhjtzlhapj7WO1M/edit?usp=sharing](https://docs.google.com/presentation/d/1hPEU4v9iFjGo6f7lVpU5kVXb9RiOyhjtzlhapj7WO1M/edit?usp=sharing)


<!-- # Links

* Gitlab pages - IT Technology courses [https://eal-itt.gitlab.io/](https://eal-itt.gitlab.io/)  
* Timeedit - Schedule [https://timeedit.net/](https://timeedit.net/)  
* Itslearning - lecture plans etc. [https://ucl.itslearning.com/](https://ucl.itslearning.com/)  
* Wiseflow - exams and OLA’s [https://ucl.wiseflow.dk/](https://ucl.wiseflow.dk/)   
* Google suite -  docs etc. - log on with your UCL edu mail [https://gsuite.google.com/dashboard](https://gsuite.google.com/dashboard)  
* UCL it guides [https://it.ucl.dk/](https://it.ucl.dk/)  
* Introduction to learning technology and IT at UCL [https://rise.articulate.com/share/bEm4fP8ZostGGxHfBfQcTErVhJWLOZLx#/](https://rise.articulate.com/share/bEm4fP8ZostGGxHfBfQcTErVhJWLOZLx#/)
* Collaboration days introduction slides [https://docs.google.com/presentation/d/1_y8RJz108hejx-MvBFnNe95Tc0-oypsiSP0LoI1m4KI/edit?usp=sharing](https://docs.google.com/presentation/d/1_y8RJz108hejx-MvBFnNe95Tc0-oypsiSP0LoI1m4KI/edit?usp=sharing)
-->
