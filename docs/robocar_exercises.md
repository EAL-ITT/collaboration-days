# Robocar øvelser og specifikationer

## Komponentdiagram

![Robocar-komponenter](./robocar_images/Components.png)

## Specifikationer for Arduino Uno-mikroprocessor

* Hjernen - CPU'en - clockhastighed 20 MHz
* Flash ram - 32KB
* SRAM - 2KB
* Sensing = GPIO - 23x

![Arduino uno](./robocar_images/auno.jpg)

## Sensorer fastgjort til robocar

* Foran og forneden - infrarød (3x) og ultralydssensor (1x) 
* Kan du pege sensorene ud på bilen?
* Bagpå - IR-modtager, USB-indgang (til programmering) og bluetooth-modtager
* En hver - kan du finde dem?


# Øvelse 9: Tid til teamwork

_ varighed: 15 minutter_: __________________________________________________ -:: 15 minutter

* Vi arbejder i vores tildelte hold
* Vælg et logo og et navn
* Mindst ét medlem skal have Helloblock installeret
* Installationsvejledning: [Helloblock Guide] (https://docs.google.com/presentation/d/1ZdSuffdbwDSBuKlol8FzCH77wLyAVI6u/edit?usp=sharing&ouid=111138532501848703089&rtpof=true&sd=true)


# Øvelse 10: Prøvekørsel

_varighed: 25 minutter_

* Hent IR-fjernbetjeningen
* Prøv at køre bilen med fjernbetjeningen
* Prøv de forskellige tilstande
* Fungerer de, som du troede, de ville gøre? 

# Øvelse 11: Viden 

_varighed: 20 minutter_

Hvis du var meget opmærksom, mens du afprøvede tilstandene, burde de næste spørgsmål være lette for dig
* Hvordan drejer den?
* Hvordan følger den en linje?
* Hvordan undgår den forhindringer?
* Svært - Hvorfor bruger den ikke et kamera til at undgå forhindringer?

**Notér jeres svar i hold, og vi vil gennemgå dem om kort tid**

# Øvelse 12: Tegning af et kredsløb

_varighed: 20 minutter_

* På et stykke papir tegner du et kredsløb, der ligner en sammenhængende vej, set oppefra
* Sving kan ikke være skarpere end 90 grader
* Du kan bruge flere stykker papir, hvis du vil

# Øvelse 13: Algoritmisk tænkeøvelse

_varighed: 25 minutter_

* Prøv at bruge fjernbetjeningen til at følge kredsløbet
* Skriv ned, hvad det gør, i trin
* F.eks. _køre ligeud, dreje til højre, dreje til venstre, hårnåle osv..._

# Øvelse 14: Programmeringsøvelse

_varighed: 45 minutter_

* Nu skal du programmere bilen
* Prøv at få bilen til at følge banen og holde sig så meget som muligt til den uden fjernbetjening

#### Eksempelblokke

![Loop](./robocar_images/ex1.png)
![Loopy loop](./robocar_images/ex2.png)
![Sport](./robocar_images/ex3.png)
![Motor](./robocar_images/ex4.png)
![Lys og lyde](./robocar_images/ex5.png)
![Sensorer](./robocar_images/ex6.png)


## USB-driver

* Nødvendig for programmering af robocar'en
* Link: [Sketchy.exe](https://drive.google.com/file/d/1AkH6xY9T6oDAF3lvBPCUBlHHe802rHEy/view?usp=sharing)
* Åbn og installer!

## Forberedelse af HelloBlock

* Kør Helloblock som admin (højreklik -> kør som admin)
* I Helloblock skal du klikke på knappen "Add extension" (tilføj udvidelse)
* Vælg BatCar
* Nu er et nyt ikon synligt i menuen i venstre rude - her er kodeblokkene til bilen


## Upload program til bilen
* Tilslut bilen med USB til computeren
* Vælg "Code mode"
* Vælg den passende COM-port 
* Upload, og så skulle dette gerne vises: 

![Downloading](./robocar_images/dl.png)


# Øvelse 15: Linje følgende øvelse

_duration: 45 minutter_

* Få bilen til at følge en linje
* Rekonstruer softwaren på billedet nederst på denne side
* Afprøv bilen på den nye bane
* Valgfrit: Brug [line following source code] (https://github.com/YahboomTechnology/Arduino-Smart-Bat-Car/blob/master/3.SDK%EF%BC%88Sourcecode%EF%BC%89/For%20Arduino%20IDE/3.Line%20Walking/Line_Walking/Line_Walking.ino) (du skal bruge Arduino IDE ) 
Du kan prøve at redigere dette, måske hvis du er god, kan du slå HelloBlock-skabelonen!

![Blocks](./robocar_images/blocks.png)


<!--
# Robocar exercises and specifications

## Components diagram

![Robocar components](./robocar_images/Components.png)

## Arduino Uno microprocessor specifications

* The brains - the CPU - clocked at a whopping 20MHz
* Flash ram - 32KB
* SRAM - 2KB
* Sensing = GPIO - 23x

![Arduino uno](./robocar_images/auno.jpg)

## Sensors attached to robocar

* Front and below – Infrared (3x) and ultrasonic sensor (1x) 
* Can you point them out on the car?
* Rear – IR receiver, USB input (for programming) and bluetooth receiver
* One each - can you find them?


# Exercise 9: Time for teamwork

_duration: 15 minutes_

* We work in our assigned teams
* Pick a logo and a name
* Minimum one member has to have Helloblock installed
* Installation guide: [Helloblock Guide](https://docs.google.com/presentation/d/1APec0TD9CbzAIQSIaI9sULXR8tdB6yx0/edit?usp=sharing&ouid=106328493335884631716&rtpof=true&sd=true)


# Exercise 10: Test drive

_duration: 25 minutes_

* Get the IR remote
* Test drive the car using the remote
* Try the different modes
* Do they function as you believed they would? 

# Exercise 11: Knowledge 

_duration: 20 minutes_

If you paid close attention while testing the modes, the next questions should be easy for you
* How does it turn?
* How does it follow a line?
* How are obstacles avoided?
* Difficult - Why does it not use a camera to avoid obstacles?

**Note your answers in teams and we will review shortly**

# Exercise 12: Drawing a circuit

_duration: 20 minutes_

* On a piece of paper, draw a circuit that looks like a connected road, as seen from above
* Turns can be no sharper than 90 degrees
* You can use more pieces of paper if you want

# Exercise 13: Algorithmic thinking exercise

_duration: 25 minutes_

* Try to use the remote to follow the circuit
* Write down what it does, in steps
* E.g. _go straight, turn right, turn left, hairpin etc..._

# Exercise 14: Programming exercise

_duration: 45 minutes_

* Now you have to program the car
* Try to make the car follow the circuit, sticking to it as much as possible without remote control

### Example blocks

![Loop](./robocar_images/ex1.png)
![Loopy loop](./robocar_images/ex2.png)
![Sport](./robocar_images/ex3.png)
![Motor](./robocar_images/ex4.png)
![Lights and sounds](./robocar_images/ex5.png)
![Sensors](./robocar_images/ex6.png)


## USB Driver

* Required for programming the robocar
* Link: [Sketchy.exe](https://drive.google.com/file/d/1AkH6xY9T6oDAF3lvBPCUBlHHe802rHEy/view?usp=sharing)
* Open and install!

## Preparing HelloBlock

* Run Helloblock as admin (right click -> run as admin)
* In Helloblock, click the ”Add extension” button
* Choose BatCar
* Now a new icon is visible in the menu on the left pane – here are the code blocks for the car


## Upload program to car
* Connect car with USB to computer
* Choose ”Code mode”
* Choose the appropriate COM port 
* Upload, and then this should show: 

![Downloading](./robocar_images/dl.png)


# Exercise 15: Line following exercise

_duration: 45 minutes_

* Get the car to follow a line
* Reconstruct the software on the image at the bottom of this page
* Test the car on the new circuit
* Optional: Use the [line following source code](https://github.com/YahboomTechnology/Arduino-Smart-Bat-Car/blob/master/3.SDK%EF%BC%88Sourcecode%EF%BC%89/For%20Arduino%20IDE/3.Line%20Walking/Line_Walking/Line_Walking.ino) (you need Arduino IDE ) 
You can try to edit this, perhaps if you are good, you can beat the HelloBlock template!

![Blocks](./robocar_images/blocks.png)
-->
