# Introduktion

Nedenfor er en samling af teambuilding-øvelser, der kan bruges til at etablere teams.

# Øvelse 1: Hvad vi deler

* varighed: 15 minutter*

Alle har et studie/deltidsjob, hobby, eller noget, som de investerer en masse fritid i.  
Denne øvelse handler om at dele dine interesser for at se, om nogen andre har lignende interesser, og for at danne et overblik over hvem vi er.

Det kan være elektronikprojekter, et spil du spiller, et kodningsprojekt, et fritidsjob, hvorfor du især har valgt IT-teknologi, eller andet interessant.

Gå til den padlet, der er linket til nedenfor, og lav et indlæg ved hjælp af *+*-knappen.

I indlægget skriver du:

* Dit fulde navn i feltet **Subject**.
* En eller flere interesser
* Et eller flere projekter, du har overvejet eller arbejdet på (behøver ikke at være IT-relateret)
* En eller flere færdigheder, som du er stolt af

Link til padlet [https://padlet.com/amni12/hvem-vi-er-it-teknolog-2024-konscidkaa1y4tj8](https://padlet.com/amni12/hvem-vi-er-it-teknolog-2024-konscidkaa1y4tj8)  


# Øvelse 2: Teamets betydning

*varighed: 30 minutter*: *: *Duration: 30 minutter*

Inden for IT er nogle af de mest efterspurgte færdigheder at kunne arbejde og kommunikere med andre.
Det er vigtigt, at du kender årsagerne til at arbejde som en del af et team.

1. Brug 10 minutter på at læse teksten på [https://eal-itt.gitlab.io/collaboration-days/why_teams/](https://eal-itt.gitlab.io/collaboration-days/why_teams/)
2. Tal med din sidemand om, hvorfor du synes, det er vigtigt at være i et team, med hensyn til den tekst, du lige har læst, og tal om, hvorfor du synes, det er vigtigt at være i et team.  Brug 10 minutter hver. Den ene person taler, og den anden lytter, mens I skriver noter om begrundelserne fra den anden person.

# Øvelse 3: Belbin-roller

*varighed: 25 minutter*

* Dr. Meredith Belbin og hendes team opdagede, at der findes ni adfærdsklynger - disse blev kaldt ' Belbin Team Roles' (se beskrivelser nedenfor).
Hvert team har brug for adgang til hver af de ni Belbin-teamrollers adfærd for at blive et højtydende team. Det betyder dog ikke, at hvert team har brug for ni personer! De fleste mennesker vil have to eller tre Belbin Team Roles, som de er mest trygge ved, og dette kan ændre sig med tiden. Hver Belbin Team Role har styrker og svagheder, og hver Team Role har lige stor betydning.* - www.belbin.com 2020

For at finde ud af, hvilke Belbin Teamroller du passer ind i, skal du følge disse trin:

1. Brug 15 minutter på at læse om de 9 forskellige Belbin-teamroller på [https://www.belbin.com/about/belbin-team-roles/](https://www.belbin.com/about/belbin-team-roles/) Tag noter om de roller, du tror, du passer ind i.
2. Brug 5 minutter til at gennemgå dine noter og begrænse dine valg af roller til de to mest passende roller.
3. Brug 5 minutter til at skrive dine to valgte teamroller i din padletpost på [https://padlet.com/amni12/hvem-vi-er-it-teknolog-2024-konscidkaa1y4tj8](https://padlet.com/amni12/hvem-vi-er-it-teknolog-2024-konscidkaa1y4tj8)

# Øvelse 4: Elevator pitch del 1

*varighed: 30 minutter*: * varighed: 30 minutter

1. Brug 5 minutter til stille og roligt at overveje, hvordan du kan præsentere dig selv på 30-60 sekunder, skriv noter, mens du tænker
2. Brug 5 minutter til at redigere din pitch, så den kun indeholder de vigtigste dele.
3. Brug 5 minutter til stille og roligt at øve din pitch
4. I grupper på 6 studerende skal I på skift præsentere jer selv ved hjælp af jeres pitch

Indholdet af din elevator pitch:

* Hvem er du? (navn og alder)
* Hvad har du tidligere gjort med hensyn til skole, job, andre studier osv.?
* Hvad motiverer dig til IT-teknologiuddannelsen ?

Begrænsninger i forbindelse med talen:

* Hold din præsentation kort og enkel, så alle kan forstå den.
* Vær personlig, engageret, autentisk og ærlig
* Varigheden af din præsentation må ikke overstige 1 minut!

# Øvelse 5: Elevator pitch del 2

* varighed: 15 minutter*

* Lav nye grupper på 6 studerende
* Skiftes til at præsentere dig selv ved hjælp af din pitch

# Øvelse 6: Holddannelse

*varighed: 45 minutter*

Regler for holddannelse 

1. Hvert team har brug for adgang til hver af de ni Belbin Team Role-adfærdsmønstre for at blive et højtydende team. Det betyder dog ikke, at hvert team kræver ni personer! De fleste mennesker vil have to eller tre Belbin Team Roles, som de er mest trygge ved

**Øvelsesinstruktioner**

1. Start med at danne grupper af 6 ved at skrive jeres navn ind i dette Sheet [https://docs.google.com/spreadsheets/d/1ZPoElm52uvdtrM5Y7GjwxCFqVyoc4lhwkH1CaDHOAkE/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1ZPoElm52uvdtrM5Y7GjwxCFqVyoc4lhwkH1CaDHOAkE/edit?usp=sharing)
2. I gruppen bruger hver 1 minut til at præsentere jeres belbin-holdroller, en person i gruppen noterer de belbin-holdroller, der er repræsenteret i gruppen.  

3. Hvis I ikke dækker alle 9 belbin-holdroller, noterer I, hvilke roller I mangler, og hvem der overlapper hinandens roller i gruppen. Derefter giver i undervisere besked, og vi prøve igen.

4. Når gruppen dækker alle 9 belbin-holdroller, har I et hold og kan fortsætte med øvelsen. 

5. Lav et fælles dokument, som alle på holdet kan få adgang til - f.eks. Google Docs.

6. I holdet skiftes i til at besvare nedenstående spørgsmål, en person tager noter i det fælles dokument:  

Hvor mange timer om ugen kan jeg investere i teamarbejdet ?  
Hvilken prioritet har teamarbejdet for mig på en skala fra 1 - 10 ?  
Hvad er dine stærke sider?  
Hvad forventer du af teamet?  
Hvordan foretrækker du at håndtere uenigheder i teamet?  
Hvilke erfaringer har du med at arbejde i et team?  
Hvad synes du er svært ved at arbejde sammen med andre?  
Hvad kan du ikke acceptere, når du arbejder sammen med andre?  
Hvad er dit drømmejob efter afslutningen af studiet?  

# Øvelse 7: Teamkontrakt

*varighed: 45 minutter*: * varighed: 45 minutter

I denne øvelse udarbejder i en teamkontrakt. Det er op til jer, hvad i medtager, og i kan bruge nedenstående som eksempel.
Husk at tage noter i jeres fælles dokument.  

Vær opmærksom på, at UCL har standarder for kontakt og adfærd, som I ikke må overtræde i jeres teamkontrakt: [https://www.mitucl.dk/studieservice/studievejledning/ordensregler](https://www.mitucl.dk/studieservice/studievejledning/ordensregler)


**Teamkontrakt**

Normer, regler og procedurer for vores hold.

* Giv holdet besked i god tid, hvis du er forsinket eller fraværende
* Holdet mødes i skoletiden
* Hvis vi er fleksible, vil teamarbejdet blive lettere
* Aftalte aftaler og deadlines skal overholdes af alle teammedlemmer
* Vi har tavshedspligt inden for teamet og bagtaler ikke andre teammedlemmer
* Vores møder har altid en dagsorden, som et af teammedlemmerne leder.
* Vi har alle et ansvar for, at dagsordenen overholdes på møderne
* Vi starter og afslutter teammøderne med en opdatering/rekapitulation
* Vi skriver referat af møderne, hver gang vi mødes
* Vi bruger den aftalte delingsplatform (google osv.) til referater af møder og fælles studiemateriale
* Vi arbejder seriøst, men giver også plads til sjov   
* Vi støtter hinanden og accepterer hinandens forskelligheder
* Der er ingen dumme spørgsmål eller idéer
* Vær ærlig!
* Arbejder koncentreret mod målet og med høj arbejdsmoral
* Døm ikke dig selv, du er god nok!
* Godt teamarbejde er fleksibilitet, gensidig respekt og anerkendelse, struktureret arbejde og en konstruktiv holdning

Mens du taler, bedes du overveje nedenstående:

* Hvordan kan vi støtte og fastholde alle medlemmer af holdet under hele undersøgelsen?
* Hvad er målet med teamarbejdet?
* Hvordan kan hvert enkelt teammedlem bidrage til at opfylde teamets mål?
* Hvordan kan vi arbejde sammen om opgaver og relationer i teamet?

# Øvelse 8: Kommunikation og møder

*varighed: 30 minutter*: * varighed: 30 minutter

I dit team bruger 30 minutter på at drøfte nedenstående spørgsmål. Husk at tage noter i jeres fælles dokument.

1. Hvor ofte skal vi mødes (minimum 2 timer en gang hver 2. uge)
2. Hvordan vil I kommunikere uden for møderne ?
3. Hvad er vigtigt at kommunikere om uden for møderne
4. Hvordan vil I dele studiemateriale, noter osv. Eksempler: Eksempler: Google drive, Gitlab, dropbox....
5. En plan for, hvordan I skiftes til at lave referater af møder og facilitere møder.


<!-- # Introduction

Below is a collection of team building exercises that can be used to establish teams.

# Exercise 1: What we share

*duration: 15 minutes*

Everybody has a hobby or something that they invest a lot of spare time in.  
This exercise is about sharing your interests to see if somebody else has similar interests.

This can be electronics projects, a game you play, a coding project, a sparetime job, why you especially have chosen IT Technology etc.

Go to the padlet linked below and make an entry using the *+* button.

In the entry write:

* Your fullname in the **Subject** field
* Your home country
* One or more interests
* One or more projects you have considered or worked on (doesn't have to be IT related)
* One or more skills you are proud of

Link to padlet [https://padlet.com/nisi2/azq3aak7fx71faez](https://padlet.com/nisi2/azq3aak7fx71faez)  


# Exercise 2: Team importance

*duration: 30 minutes*

In IT some of the most sought after skills are being able to work and communicate with others.
It is important that you know the reasons behind working as a part of a team.

1. Use 10 minutes to read the text at [https://eal-itt.gitlab.io/collaboration-days/why_teams/](https://eal-itt.gitlab.io/collaboration-days/why_teams/)
2. With the person sitting next to you talk about why you find it important to be in a team, regarding the text you just read.  Use 10 minutes each. One persons talks and the other listens while writing notes about the reasons from the other person.

# Exercise 3: Belbin roles

*duration: 25 minutes*

*Dr Meredith Belbin and his team discovered that there are nine clusters of behaviour - these were called ' Belbin Team Roles' (see descriptions below).
Each team needs access to each of the nine Belbin Team Role behaviours to become a high performing team. However, this doesn't mean that every team requires nine people! Most people will have two or three Belbin Team Roles that they are most comfortable with, and this can change over time. Each Belbin Team Role has strengths and weaknesses, and each Team Role has equal importance.* - www.belbin.com 2020

To find out which belbin team roles you fit into follow these steps:

1. Use 15 minutes to read about the 9 different belbin team roles at [https://www.belbin.com/about/belbin-team-roles/](https://www.belbin.com/about/belbin-team-roles/) Take notes about the roles you think you fit in to.
2. Use 5 minutes to review your notes and constrain your role choices to the two most fitting roles.
3. Use 5 minutes to write your two chosen team roles in your padlet entry at [https://padlet.com/nisi2/azq3aak7fx71faez](https://padlet.com/nisi2/azq3aak7fx71faez)

# Exercise 4: Elevator pitch part 1

*duration: 30 minutes*

1. Use 5 minutes to quietly consider how you can present yourself in 30-60 seconds, write notes while thinking
2. Use 5 minutes to edit your pitch to only include the most important parts.
3. Use 5 minutes to quietly rehearse your pitch
4. In groups of 8-10 students (211 + 212 class can not be mixed), take turns presenting yourself using your pitch

Contents of your elevator pitch:

* Who are you ? (Name and age)
* What have you done previously regarding school, job, other studies etc. ?
* What motivates you towards the IT Technology education ?

Constraints of the pitch:

* Keep your presentation short and simple in order for everybody to understand it.
* Be personal, engaged, authentic and honest
* The duration of your presentation may not exceed 1 minute!

# Exercise 5: Elevator pitch part 2

*duration: 15 minutes*

* Make new groups of 8-10 students (211 + 212 class can not be mixed)
* Take turns presenting yourself using your pitch

# Exercise 6: Team forming

*duration: 45 minutes*

Rules for forming teams 

1. Each team needs access to each of the nine Belbin Team Role behaviours to become a high performing team. However, this doesn't mean that every team requires nine people! Most people will have two or three Belbin Team Roles that they are most comfortable with

2. Teams can not be mixed between 211 and 212 class

**Exercise instructions**

1. Start by gathering in groups of 8. 
2. In the group use 1 minute each to present your belbin team roles, one person in the group notes the belbin team roles represented in the group.  

3. If you do not cover all 9 belbin team roles note which roles you are missing and notify a teacher.

4. If the group covers all 9 belbin team roles you have a team and can continue with the exercise. 

5. Make a shared document that everybody in the team can access

6. In the team take turns to answer the below questions, one person takes notes in the shared document:  
How many hours a week can I invest in the team work ?  
What priority does team work have for me on a scale from 1 - 10 ?  
What are your strengths ?  
What do you ecpect from the team ?  
How do you prefer to handle disagreements in the team ?  
What are your experiences working in a team ?  
What do you find hard when working together with others ?  
What can't you accept when working with others ?  
What is your dream job after completing the study ?  

# Exercise 7: Team contract

*duration: 45 minutes*

In this exercise you create a team contract, what you include is up to you and you can use the below as an example.
Remember to take notes in your shared document.  

Please note that UCL has standards for contact and behaviour that you are not allowed to violate in your team contract:  [https://www.myucl.dk/student-services/standards-for-contact-and-behaviour](https://www.myucl.dk/student-services/standards-for-contact-and-behaviour)


**Team Contract**

Norms, rules and procedures for our team.

* Notify the team in due time if you are delayed or absent
* The team will meet during school hours
* If we are flexible team work will be easier
* Agreed appointments and deadlines must be met by all team members
* We have confidentiality within the team and do not slander about other team members
* Our meetings always have an agenda which is facilitated by one of the team members
* We all have a responsibility that the agenda is fulfilled at meetings
* We start and end team meetings with an update/recap
* We write minutes of meetings everytime we meet
* We use the agreed sharing platform (google etc.) for minutes of meetings and shared study material
* We work seriously but also allow space for fun   
* We support each other and accept each others differences
* There are no stupid questions or ideas
* Be honest!
* Work concentrated towards the goal and with high work ethic
* Don't judge yourself, you are good enough!
* Good team work is flexibility, mutual respect and recognition, structured work and a contructive attitude

While talking please consider the below:

* How can we support and retain all members of the team throughout the study ?
* What the goal of the team work is ?
* How each team member can contribute to fullfill the team goals ?
* How can we work together with tasks and relations within the team ?

# Exercise 8: Communication and meetings

*duration: 30 minutes*

In your team use 30 minutes to discuss the below questions. Remember to take notes in your shared document.

1. How often we are going to meet (minimum is 2 hours once every 2 weeks)
2. How are you going to communicate outside of meetings ?
3. What is important to communicate about outside of meetings
4. How will you share study material, notes etc. Examples: Google drive, Gitlab, dropbox....
5. A plan for taking turns in doing minutes of meetings and facilitating meetings.
-->
