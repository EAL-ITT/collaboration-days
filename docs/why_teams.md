# Hvorfor teams?

**Dette kapitel præsenterer forskellene og grunden til at bruge teams aktivt på din uddannelse.**

Der findes en bog om teams i videregående uddannelser på dansk: [Annelise Dahlbæk - Studie Gruppen] (https://dpf.dk/produkt/bog/studiegruppen-2-udgave)  
Det anbefales at læse og bruge bogen i teamarbejdet og vil give dig en dybere forståelse af teams.  
Nedenstående tekst opsummerer teori fra bogen, sammen med it-teknologilærernes erfaringer.

## Elev vs. studerende

At gå på en videregående uddannelse er anderledes end at være elev på lavere uddannelsesniveauer.  
Kort fortalt er en elev motiveret af en lærer - læreren tager initiativ til og planlægger alle aktiviteter for eleven. Målene er meget klare. 

Den studerende er derimod motiveret af sig selv - underviseren tilbyder teori og øvelser, men ansvaret for at studere/lære ligger hos den studerende.  
Målene er ofte vanskeligere at forstå og kræver evne til at reflektere og fortolke, for at kunne sætte målene ind i en sammenhæng.
Målene for IT-teknologiuddannelsen er beskrevet i [læseplanen] (https://www.ucl.dk/link/5faca5b7ecb34711818e580e733ef48e.aspx?nocache=3fc654a6-fd18-4458-87f5-32b6d74ddada)

**Forskelle mellem elev og studerende**

| Fra | Til |
|--- |--- | 
| At være elev | At være studerende |
| At lave lektier | At studere | 
| At arbejde med fag | At undersøge problemer eller spørgsmål | 
| Viden og mening | Undersøge og anvende |
| Tale om | Argumentere professionelt |
| Stram tidsplan | Eget ansvar for planlægning |

Det kan være en stor udfordring at gå fra elev til studerende, og der er stor sandsynlighed for, at du vil føle dig overvældet og alene med dit nye ansvar.

Ved at være en aktiv del af et team får du mulighed for at dele og lære, hvordan andre i samme situation håndterer forandringen.

## Peer learning

*"Jo mere eleverne er involveret, fagligt og socialt, i fælles læringsoplevelser, der forbinder dem som lærende med deres jævnaldrende, jo mere sandsynligt er det, at de bliver mere involveret i deres egen læring og investerer den tid og energi, der er nødvendig for at lære "* ([Tinto, 1997](https://www.jstor.org/stable/2959965?seq=1)).

## Professionel karriere

Inden for IT-teknologi vil du for det meste arbejde i teams bestående af personer med forskellige kompetencer.   
Kernekompetencer i IT-professionen er at kunne kommunikere, samarbejde og dokumentere sit arbejde.  
Disse kompetencer overses og forsømmes ofte af studerende, og den typiske tilgang fra nye it-teknologistuderende er at fokusere på at beherske specifikke teknologier.  
Da teknologierne ændrer sig hurtigt, fokuserer IT-teknologuddannelsen på at uddanne de studerende til at kunne lære nye teknologier i løbet af deres karriere.   

At være med i et team kan hjælpe dig med at erhverve og øve dig i de ovennævnte kernekompetencer. 

## Ny i byen

Mange af vores studerende kommer fra en anden by i Danmark eller endda fra et andet land.
Det kan være svært at etablere sig, når alt er nyt. Ny by, ny skole, ny lejlighed, nye sociale relationer osv. 

Når du er en del af et hold, har du et sted at høre til fra dag 1.  
At etablere et stærkt team kan gøre forskellen på, om du gennemfører dit studie eller ej!

# Hvad er et team?

Et hold er en gruppe på 6 studerende, der er kombineret fra forskellige [belbin](https://www.belbin.com/about/belbin-team-roles/) holdroller.  

Fra [belbin](https://www.belbin.com/about/belbin-team-roles/):  
*Hvert team har brug for adgang til hver af de ni Belbin Teamrolle-adfærd for at blive et højtydende team. Det betyder dog ikke, at hvert team kræver ni personer! De fleste mennesker vil have to eller tre Belbin Team Roles, som de er mest trygge ved, og dette kan ændre sig med tiden. Hver Belbin-teamrolle har styrker og svagheder, og hver teamrolle har lige stor betydning.*

## Teamopgaver

Et team løser opgave i forbindelse med undersøgelsen. Eksempler på opgaver er:

* Forståelse af en ny teknologi
* Læse dokumentation
* Planlægning af udvikling inden for projektforelæsninger
* Samarbejde om og opgave fra netværksundervisning
* Hjælpe hinanden med at tilegne sig viden

Det vigtige at bemærke er, at det er teamet, der bestemmer, hvilke opgaver der er relevante for teamet. Normalt er samarbejde i teamet om en opgave, en god idé, hvis et eller flere teammedlemmer, har et behov for det.
Holdet kan også få opgaver direkte fra en underviser, disse opgaver vil være klasseopgaver og skemalagt til jer.

## Holdmøder

* Teammøder skal være meningsfulde og skabe værdi for teamet og uddannelsen.
* Teammøder er en planlagt, tidsbegrænset interaktion mellem teammedlemmerne, hvor målet er at opnå ting sammen. 

Et team skal holde et møde mindst en gang hver anden uge. På dit studie vil du have tid om eftermiddagen til at planlægge dit teammøde.
Holdmødet er centralt for at være en aktiv og effektiv studiegruppe. Det er ikke let at afholde møder, og det kræver disciplin, planlægning og facilitering.
At deltage i et teammøde kræver, at man er opmærksom på dagsordenen og formidleren, lytter aktivt og forsøger at forstå de andre teammedlemmers synspunkter.   

Før et teammøde skal teamet udarbejde en streng dagsorden, som nedenfor er et eksempel:  

**Område:** Hold 2-møde - 11/12-2022, kl. 13:00 - 15:00  
**Sted:** På Marikas plads, Eksempelvej 40, 5000 Odense C

| Emne (ansvarlig) | Handlinger | Proces | Teknikker | Tid | Beslutning |
|---|---|---|---|---|---|
| Start & status (John) | Opsummering fra sidste møde, aftaler osv.  | Lytte | John taler, teammedlemmer reflekterer individuelt og spørger, hvad overrasker mig? Kort resumé | 5 min.  |
| Foredrag | Vi skal udfylde den opgave, som foredragsholderen har givet os | Rundbordssamtaler - alles synspunkter. Efter fri snak | Parvis: Løs de valgte delopgaver. Alle: Præsentation af resultater | 50 min.  | |
| Forståelse | Det datablad for mikrocontrollere, som vi får i forelæsningerne om indlejret teknologi, er ret skræmmende. Som et team skal vi skabe overblik og finde ud af, hvilke spørgsmål vi skal stille forelæseren.  | Refleksion og fri snak | Individuelt reflekterer vi over, hvilke dele der er sværest at forstå, og noterer udkast til spørgsmål. Derefter præsenterer vi vores spørgsmål og finpudser/kombinerer dem | 50 min |
| Eventuelt andet | | | | 10 min. | |

#### Teamfacilitator

En teamfacilitator styrer team-mødets forløb på en objektiv, anerkendende, konstruktiv og ikke manipulerende måde.  
En teamfacilitator kan i nogle situationer være en foredragsholder og i andre situationer et teammedlem. Hvis facilitatoren er et teammedlem, er han/hun en ligeværdig facilitator, hvilket betyder, at facilitatoren tager ansvar for mødets konklusioner.  
En foredragsholder som facilitator bidrager derimod ikke til konklusionerne, men er blot facilitator.  

Teamfacilitatoren kan bidrage med værdi og mening ved at sikre, at teammøderne er relevante for undersøgelsen. Desuden skal facilitatoren sikre, at alle teammedlemmer får mulighed for at bidrage aktivt i overensstemmelse med deres forudsætninger og potentiale.  

At være facilitator er at kontrollere og have en form for magt. Det er vigtigt, at facilitatoren er bevidst om, at han/hun har magt, og at han/hun sikrer, at han/hun er objektiv og retfærdig. Facilitatoren har overblikket under møderne og skal være opmærksom på den potentielle fare for andre magtforhold mellem teammedlemmerne.  
Det er vigtigt, at magtforhold ikke hæmmer mødet på nogen måde.

Facilitatorens fokus er at lede teamet gennem arbejdsprocesser, nå målene og skabe et effektivt miljø for teamet at arbejde i.  
Facilitatoren er også en guide for teamet, der hjælper dem med at analysere, observere, lytte aktivt, stille spørgsmål, afklare og skabe fremskridt.

I løbet af 1. semester vil teamets facilitatorrolle skifte mellem hvert teammøde for at sikre, at alle teammedlemmer får erfaring med og forståelse af facilitatorrollen.

## Inddragelse og disciplin

Et effektivt team har engagerede teammedlemmer, som tager teamets aktiviteter alvorligt.  
Alle teammedlemmer skal bidrage til teamarbejdet, og alle teammedlemmer skal tillade andre medlemmers bidrag på trods af forskelle og færdighedsniveau.

Det er afgørende for et effektivt og seriøst team at overholde aftaler inden for teamet, og det kræver, at man er engageret i at overholde de aftaler, der er indgået inden for teamet, når man arbejder i branchen.
Holdets succes er afgørende for det enkelte holdmedlems succes, både i forbindelse med dine studier og senere i din professionelle karriere. 


<!-- # Why teams

**This chapter presents the differences and reason for using teams actively at your education.**

A book about teams in higher education exists in danish: [Annelise Dahlbæk - Studie Gruppen](https://dpf.dk/produkt/bog/studiegruppen-2-udgave)  
Reading and using the book in team work is recommended and will give you a deeper understanding of teams.  
The below text summarize theory from the book, along with the IT technology lecturers experience.

## Pupil vs. Student

Attending higher education is different from being a pupil at lower education levels.  
In brief a pupil is motivated by a teacher - the teacher initiates and schedules all activities for the pupil. Goals are very clear. 

The student on the other hand is motivated by himself - the lecturer offers theory and exercises, but the responsibility for studying/learning belongs to the student.  
The goals are often harder to comprehend and requires the ability to reflect and interpret, to be able to put the goals into context.
The goals of the IT technology education are described in the [curriculum](https://www.ucl.dk/link/5faca5b7ecb34711818e580e733ef48e.aspx?nocache=3fc654a6-fd18-4458-87f5-32b6d74ddada)

**Pupil vs. Student differences**

| From | To  |
|--- |--- |
| Being a pupil | Being a student|
| Doing homework | Studying |
| Working with subjects | Examining problems or issues | 
| Know and mean | Examine and apply |
| Talk about | Argue professionally |
| Strict schedule | Own responsibility for planning |

It can be quite challenging going from pupil to student, chances are that you will feel overwhelmed and alone with your new responsibilities.

Being an active part of a team gives you the opportunity to share and learn how others in the same situation deals with the transformation.

## Peer learning

*"The more students are involved. academically and socially, in shared learning experiences that link them as learners with their peers, the more likely they are to become more involved in their own learning and invest the time and ebergy needed to learn"* ([Tinto, 1997](https://www.jstor.org/stable/2959965?seq=1))

## Professional career

In the IT technology profession you will mostly work in teams consisting of people with different skills.   
Core competences in the IT profession is to be able to communicate, collaborate and document your work.  
These competences are often overlooked and neglected by students, the typical approach from new IT technology students is focus on mastering specific technologies.  
Because technologies changes rapidly, the IT technology education focuses on educating students to be able to pick up new technologies throughout their career.   

Being in a team can help you aquire and practice the above mentioned core competences. 

## New in town

Many of our students comes from a different town in Denmark or even from a different country.
It can be hard to establish yourself when everything is new. New town, new school, new appartment, new social relations etc. 

When you are a part of a team you have a place to belong from day 1.  
Establishing a strong team can make the difference in completing your study or not!

# What is a team ?

A team is a group of 8 students that are combined from different [belbin](https://www.belbin.com/about/belbin-team-roles/) team roles.  

From [belbin](https://www.belbin.com/about/belbin-team-roles/):  
*Each team needs access to each of the nine Belbin Team Role behaviours to become a high performing team. However, this doesn't mean that every team requires nine people! Most people will have two or three Belbin Team Roles that they are most comfortable with, and this can change over time. Each Belbin Team Role has strengths and weaknesses, and each Team Role has equal importance.*

## Team tasks

A team solves task related to the study. Examples of tasks are:

* Understanding a new technology
* Reading documentation
* Planning development within project lectures
* Collaborating on and assignement from networking class
* Helping each other to gain knowledge

The important thing to notice is that it is the team that decides what tasks are relevant for the team. Usually collaboration in the team about a task, is a good idea if one or more team members, has a need for it.
The team may also be given tasks directly from a lecturer, these task will be in class tasks and scheduled for you.

## Team meetings

* Team meetings needs to be meaningfull and create value for the team and the education.
* Team meetings are a planned, time restricted interaction between the team members, the goal is to accomplish things together. 

A team will have a meeting at least once every two weeks. In your study you will have time in the afternoons to plan you team meeting.
The team meeting is central to being an active and effecient study group. Conducting meetings is not an easy thing to do and requires disipline, planning and facilitation.
Attending a team meeting requires, paying attention to the agenda and the facilitator, listening actively and trying to understand other team members point of view.   

Before a team meeting the team will have to make a strict agenda, below is an example:  

**Subject:** Team 2 meeting - 20201112, 13:00 - 15:00  
**Location:** At Marikas place, Eksempelvej 40, 5000 Odense C

| Subject (responsible)  | Actions  | Process  | Techniques  | Time  | Decision  |
|---|---|---|---|---|---|
|  Start & status (John) | Recap from last meeting, agreements etc.  | Listening  | John talks, team members reflects individually asking what surprises me? Short summary  | 5 min.  |   |
| Lectures 20201116  | We need to complete the assigntment given by the lecturer  | Round table - everybodys point of view. After free talking  | Pairs: Solve chosen assignment sub tasks. All: Presentation of results   | 50 min.  |   |
| Understanding  | The microcontroller datasheet given to us in embedded lectures is quite daunting. As a team we need to establish an overview and identify which questions to ask the lecturer.  | Reflection and free talk  | Individually we reflect on which parts are the hardest to understand and note draft questions. After we do present our questions and refine/combine them | 50 min  |   |
| Any other business |  |  |  | 10 min. |  |

### Team facilitator

A team facilitator controls the progress of the team meeting in an objective, appreciatively, constructive and not manipulative way.  
A team facilitator can in some situations be a lecturer and other situations a team member. If the facilitator is a team member thay are an equal facilitator, meaning that the facilitator takes responsibility regarding the conclusions of the meeting.  
A lecturer as facilitator on the other hand does not contribute to conclusions but merely facilitates.  

The team facilitator can contribute with value and meaning by ensuring that team meetings are relevant to the study. Becides that the facilitator must ensure that every team member gets the chance to contribute actively according to their prerequisites and potential.  

Being a facilitator is to control and have a form of power. It is important that the facilitator is aware of being in power, ensuring to be objective and fair. The facilitator has the overview during meetings and needs to be aware of the potential danger of other power relations between team members.  
It is important that power relations doesn't inhibit the meeting in any way.

The focus of the facilitator is to guide the team through work processes, reach goals and create an effecient environment for the team to work in.  
The facilitator is also a guide for the team helping them to analyze, observe, listen actively, question, clarify and create progress.

During 1st semester the team facilitator role will change between every team meeting to ensure that every team member gains experience with and understanding of the facilitator role.

## Involvement and discipline

An efficient team has involved team members who take the teams activities serious.  
Every team member has to contribute to the team work and every team member has to allow the contribution from other members, despite differences and skill level.

Keeping agreements within the team is vital to an efficient and serious team, working in the industry requires you to be dedicated to the agreements made within the team.
The success of the team is key to the individual team member's success, both in your studies and later professionel career. 

-->
